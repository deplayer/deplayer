import * as types from '../../constants/ActionTypes'
import Button from '../common/Button'
import { Translate } from 'react-redux-i18n'
import Icon from '../common/Icon'
import SongRow from '../MusicTable/SongRow'
import CoverImage from '../MusicTable/CoverImage'
import { State as CollectionState } from '../../reducers/collection'
import { State as QueueState } from '../../reducers/queue'
import { Dispatch } from 'redux'

interface Album {
  id: string
  name: string
  year?: number
}

type AlbumProps = {
  album: Album,
  queue: QueueState,
  songs: Array<string>,
  collection: CollectionState,
  dispatch: Dispatch
}

const Album = (props: AlbumProps) => {
  const albumId = props.album.id

  const extractSongs = () => {
    if (!props.songs) {
      return null
    }

    // Create a Set of unique song IDs
    const uniqueSongsSet = new Set(props.songs)

    // Log any duplicates that were removed
    if (uniqueSongsSet.size !== props.songs.length) {
      const duplicates = props.songs.filter(songId => 
        props.songs.indexOf(songId) !== props.songs.lastIndexOf(songId)
      )
      console.warn(`Removed ${props.songs.length - uniqueSongsSet.size} duplicate songs from album ${props.album.name} (${albumId}):`, 
        [...new Set(duplicates)])
    }

    // Group songs by disc number using Map to ensure unique entries per disc
    const songsByDiscMap = new Map<number, Set<string>>()
    
    for (const songId of uniqueSongsSet) {
      const song = props.collection.rows[songId]
      if (!song) {
        console.warn(`Song ${songId} not found in collection for album ${props.album.name}`)
        continue
      }
      
      const discNumber = song.discNumber || 1
      if (!songsByDiscMap.has(discNumber)) {
        songsByDiscMap.set(discNumber, new Set())
      }
      songsByDiscMap.get(discNumber)?.add(songId)
    }

    // Convert Map of Sets to sorted array structure
    const sortedDiscs = Array.from(songsByDiscMap.keys()).sort((a, b) => a - b)

    return sortedDiscs.map(discNumber => {
      const discSongsSet = songsByDiscMap.get(discNumber) || new Set<string>()
      const discSongs = Array.from(discSongsSet)
        .sort((a, b) => {
          const songA = props.collection.rows[a]
          const songB = props.collection.rows[b]
          // Sort by track number if available, otherwise by title
          if (songA.track && songB.track) {
            return songA.track - songB.track
          }
          if (songA.title < songB.title) return -1
          if (songA.title > songB.title) return 1
          return 0
        })
        .map((songId) => {
          const songRow = props.collection.rows[songId]
          return (
            <SongRow
              mqlMatch={false}
              disableCovers
              style={{}}
              key={songId}
              dispatch={props.dispatch}
              isCurrent={false}
              slim={true}
              onClick={() => {
                props.dispatch({ type: types.ADD_ALBUM_TO_QUEUE, albumId })
              }}
              song={songRow}
            />
          )
        })

      // Only show disc header if there are multiple discs
      const showDiscHeader = songsByDiscMap.size > 1

      return (
        <div key={discNumber} className="mb-4">
          {showDiscHeader && (
            <h4 className="text-lg font-medium mb-2 px-4">
              <Icon icon="faCompactDisc" className="mr-2" />
              <Translate value="album.disc" /> {discNumber}
            </h4>
          )}
          {discSongs}
        </div>
      )
    })
  }

  return (
    <div 
      className='mx-0 z-4 flex flex-col md:flex-row items-center md:items-start mb-16' 
      key={albumId}
    >
      <div className='sticky backdrop-blur-sm bg-black/30 md:bg-transparent shadow-lg md:shadow-none py-4 top-0 mt-0 w-full md:w-56 flex md:flex-col items-center md:mx-8 z-10'>
        <div
          className='h-56 w-56 mb-2 p-4 md:h-56 md:w-56 cursor-pointer md:mr-4'
          onClick={() => {
            props.dispatch({ type: types.ADD_ALBUM_TO_QUEUE, albumId })
          }}
        >
          <CoverImage
            cover={
              props.collection.rows[Array.from(new Set(props.songs))[0]]?.cover
            }
            size='thumbnail'
            albumName={props.album.name}
          />
        </div>
        <div>
          <h3 className='text-lg p-4 w-56'>{props.album.name}</h3>
            { props.album.year && <h4 className='text-md p-4 w-56'>{props.album.year}</h4>}
          <Button
            transparent
            onClick={() => {
              props.dispatch({ type: types.ADD_ALBUM_TO_QUEUE, albumId })
            }}
          >
            <Icon
              icon='faFolderPlus'
              className='mx-2'
            />
            <Translate value='buttons.addToQueue' />
          </Button>

          {props.queue?.currentPlaying && (
            <Button
              transparent
              onClick={() => {
                // Ensure unique songs when adding to queue
                const uniqueSongIds = Array.from(new Set(props.songs))
                const songs = uniqueSongIds.map(songId => props.collection.rows[songId])
                props.dispatch({ type: types.ADD_TO_QUEUE_NEXT, songs })
              }}
            >
              <Icon
                icon='faPlusCircle'
                className='mx-2'
              />
              <Translate value='buttons.addNext' />
            </Button>
          )}

          <Button
            transparent
            onClick={() => {
              props.dispatch({ type: types.PIN_ALBUM, albumId })
            }}
          >
            <Icon
              icon='faFolderPlus'
              className='mx-2'
            />
            <Translate value='buttons.pinAlbum' />
          </Button>
        </div>
      </div>
      <div className='w-full m-2'>
        {extractSongs()}
      </div>
    </div>
  )
}

export default Album
