import { render, screen, fireEvent } from '@testing-library/react'
import { describe, it, expect, vi } from 'vitest'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { configureStore } from '@reduxjs/toolkit'
import Media from '../../../entities/Media'
import Artist from '../../../entities/Artist'
import Album from '../../../entities/Album'
import SongRow from './index'
import type { Props } from './index'

// Create a mock store
const createMockStore = () => configureStore({
  reducer: {
    favorites: (state = { favoriteIds: new Set() }) => state,
  }
})

const setup = (overrideProps = {}) => {
  const mockOnClick = vi.fn()
  const props: Props = {
    dispatch: (_action: any) => _action,
    song: new Media({
      id: 'artist-name-album-name-undefined-title',
      title: 'title',
      cover: { thumbnailUrl: 'thumbnail', fullUrl: '' },
      artist: new Artist({
        id: '1',
        name: 'artistName'
      }),
      album: new Album({
        id: 'foo',
        name: 'album',
        artist: new Artist({
          id: 'foo',
          name: 'foo'
        }),
        thumbnailUrl: 'thumbnail'
      }),
      artistName: 'artistName',
      albumName: 'album',
      type: 'audio',
      duration: 100,
      stream: {},
      genres: []
    }),
    queue: {
      trackIds: [],
      randomTrackIds: [],
      currentPlaying: null,
      repeat: false,
      shuffle: false,
      nextSongId: null,
      prevSongId: null,
    },
    onClick: mockOnClick,
    isCurrent: false,
    style: {},
    disableAddButton: false,
    disableCovers: false,
    mqlMatch: true,
    slim: false,
    ...overrideProps
  }

  const store = createMockStore()

  const utils = render(
    <Provider store={store}>
      <Router>
        <SongRow {...props} />
      </Router>
    </Provider>
  )
  return { ...utils, props, mockOnClick }
}

describe('SongRow', () => {
  it('should show render without errors', () => {
    setup()
    expect(screen.getByTestId('song-row')).toBeTruthy()
  })

  it('should trigger onClick when cover image is clicked', () => {
    const { mockOnClick, container } = setup()
    const coverElement = container.querySelector('[data-testid="song-cover"]')
    expect(coverElement).toBeTruthy()
    fireEvent.click(coverElement!)
    expect(mockOnClick).toHaveBeenCalledTimes(1)
  })
})
