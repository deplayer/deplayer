export interface FormField {
  title: string;
  type: string;
  name?: string;
  value?: string | boolean;
}
