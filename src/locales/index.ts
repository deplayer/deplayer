import en from './en.json'
import ca from './ca.json'
import es from './es.json'

export default {
  en,
  ca,
  es
}
