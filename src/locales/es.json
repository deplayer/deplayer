{
  "menu": {
    "home": "Inicio",
    "search": "Buscar",
    "searchPlaceholder": "Busca artistas, álbumes, canciones o comandos...",
    "searching": "Buscando...",
    "noResults": "No se encontraron resultados",
    "navigate": "Navegar",
    "select": "Seleccionar",
    "close": "Cerrar",
    "import": "Importar",
    "collection": "Colección",
    "queue": "Cola",
    "artists": "Artistas",
    "playlists": "Listas",
    "favorites": "Favoritos"
  },
  "titles": {
    "albums": "Álbumes",
    "relatedAlbums": "Álbumes del mismo artista",
    "mostPlayedSongs": "Canciones más reproducidas",
    "sameGenreSongs": "Canciones del mismo género",
    "genrePlaylists": "Listas por Género"
  },
  "labels": {
    "sync": "Sincronizar",
    "enableSync": "Activar sincronización",
    "syncServerUrl": "URL del servidor de sincronización",
    "syncDescription": "Sincroniza tus datos entre dispositivos usando una base de datos PostgreSQL.",
    "syncServerInstructions": "Para configurar tu propio servidor de sincronización, sigue las instrucciones en nuestra documentación.",
    "readDocs": "Leer la documentación",
    "repl": "REPL",
    "actions": "Acciones",
    "language": "Idioma",
    "useSystemLanguage": "Usar idioma del sistema",
    "enableReactPlayer": "usar react-player (beta)",
    "enableSpectrum": "espectro del reproductor",
    "deleteCollection": "Eliminar colección",
    "exportCollection": "Exportar colección",
    "importCollection": "Importar colección",
    "deleteSettings": "Eliminar configuración",
    "addProviders": "Añadir proveedores",
    "providers": "Proveedores",
    "lazyProviders": "Proveedores de búsqueda",
    "generalSettings": "Configuración",
    "jellyfin": "Jellyfin",
    "jellyfin.baseUrl": "URL del servidor",
    "jellyfin.username": "Nombre de usuario",
    "jellyfin.apiKey": "Clave API",
    "musicbrainz": "MusicBrainz",
    "enabled": "Activado",
    "settingsDescription": "Configura los ajustes y preferencias de tu reproductor multimedia",
    "storyBehind": "Historia Detrás"
  },
  "song": {
    "label": {
      "song": "Canción",
      "title": "Título",
      "album": "Álbum",
      "artist": "Artista",
      "time": "Duración",
      "genre": "Género",
      "dateAdded": "Fecha añadido",
      "price": "Precio",
      "played": "Reproducido",
      "times": "veces"
    }
  },
  "placeholder": {
    "search": "Escribe algún artista, canción o álbum..."
  },
  "notifications": {
    "songPinned": "Canción fijada",
    "songUnpinned": "Canción desfijada",
    "search": {
      "finished": "¡Búsqueda finalizada!"
    },
    "settings": {
      "saved": "¡Configuración guardada!"
    }
  },
  "buttons": {
    "save": "Guardar",
    "addNext": "Añadir siguiente",
    "addNewMedia": "Añadir nuevo contenido",
    "addProvider": "Configurar nuevo proveedor",
    "playAll": "Reproducir esta lista",
    "clearQueue": "limpiar cola",
    "saveAsPlaylist": "guardar como lista de reproducción",
    "addToQueue": "Añadir a la cola",
    "removeFromCollection": "eliminar de la colección",
    "removeFromQueue": "Eliminar de la cola",
    "shuffle": "Aleatorio",
    "hidePlayer": "Ocultar",
    "repeat": "Repetir",
    "fullScreen": "Pantalla completa",
    "toggleMiniQueue": "Mostrar/ocultar cola",
    "startPlaying": "Comenzar reproducción",
    "toggleSpectrum": "Mostrar/ocultar espectro",
    "toggleVisuals": "Mostrar/ocultar visuales",
    "downloadMedia": "Descargar contenido",
    "pinAlbum": "Fijar álbum",
    "syncProvider": "Sincronizar toda la música",
    "saveSettings": "Guardar configuración"
  },
  "message": {
    "noCollectionItems": "La colección está vacía, aquí verás todo el contenido descubierto por Deplayer.\nPara empezar a llenar tu colección necesitas añadir contenido o añadir un proveedor y comenzar a buscar.",
    "queueEmpty": "La cola está vacía. ¡Añade algunas canciones para empezar a reproducir!",
    "noMostPlayed": "Aún no has reproducido ninguna canción",
    "goToCollection": "¡Ve a la colección para reproducir algo!",
    "tryDemoSong": "Probar canción de demostración",
    "addSongsFromCollection": "Añade canciones desde la colección o busca nuevas",
    "jumpToCollection": "Ir a la colección",
    "noPlaylists": "No hay listas de reproducción",
    "createPlaylistHint": "Crea tu primera lista añadiendo canciones a la cola y guardándola",
    "addSongsToQueue": "Añade canciones a la cola para crear tu primera lista",
    "goToQueue": "Ir a la Cola",
    "startSearch": "Empezar a Buscar",
    "addProvider": "Añadir Proveedor",
    "startSearchingForMusic": "Empieza a buscar música para añadir a tu colección",
    "addSearchableProvider": "Añade un proveedor de búsqueda para empezar a descubrir música"
  },
  "peer": {
    "connectedPeers": "Pares conectados",
    "joinRoom": "Unirse a la sala",
    "shareRoom": "Compartir sala",
    "listenAlong": "Escuchar juntos",
    "enterRoomCode": "Introducir código de sala",
    "enterUsername": "Introducir nombre de usuario",
    "nowPlaying": "Reproduciendo ahora",
    "notPlaying": "No reproduciendo",
    "leaveRoom": "Salir de la sala",
    "roomLink": "Enlace de la sala",
    "createRoom": "Crear sala",
    "request": "Descargar",
    "streamRealtime": "Transmisión en directo",
    "leave": "Salir de la sala",
    "joinOrCreateRoom": "Unirse o crear una sala",
    "join": "Entrar"
  },
  "dashboard": {
    "recentlyAdded": "Álbumes añadidos recientemente",
    "welcome": {
      "title": "¡Hola <i>audiófilo</i>! Bienvenido a",
      "description": "Accede a tu biblioteca de música y disfrútala cuando la necesites.",
      "steps": "Para empezar a reproducir contenido, sigue uno de los siguientes pasos:",
      "setupProviders": "Configura tus proveedores de medios (Subsonic API, mstream o ITunes)",
      "addMedia": "Añade nuevos medios a tu colección",
      "addMediaDescription": "Webtorrent, Sistema de archivos, IPFS o youtube-dl-server",
      "goToCollection": "O ve a tu colección",
      "authenticated": "Estás autenticado con tu clave de acceso",
      "authNeeded": "Accede a las funcionalidades sociales autenticándote con tu clave de acceso",
      "authButton": "🔒 Autenticación"
    }
  },
  "sidebar": {
    "providers": "Proveedores",
    "openSource": "es código abierto!",
    "showCode": "Muéstrame el código",
    "buyMeACoffee": "Invítame a un café",
    "supportProject": "Apoya este proyecto"
  },
  "commandBar": {
    "placeholder": "Escribe una orden o busca...",
    "categories": {
      "commands": "Órdenes",
      "songs": "Canciones",
      "albums": "Álbumes",
      "artists": "Artistas",
      "playlists": "Listas de reproducción",
      "navigation": "Navegación",
      "themes": "Temas",
      "peers": "Pares"
    },
    "commands": {
      "goToArtists": "Ir a Artistas",
      "goToAlbums": "Ir a Álbumes",
      "goToQueue": "Ir a la Cola",
      "goToPlaylists": "Ir a Listas de reproducción",
      "goToSettings": "Ir a Configuración",
      "goToExplore": "Ir a Explorar",
      "togglePlaying": "Alternar Reproducción",
      "playNext": "Reproducir Siguiente",
      "playPrevious": "Reproducir Anterior"
    }
  },
  "languages": {
    "english": "Inglés",
    "spanish": "Español",
    "catalan": "Catalán",
    "selectLanguage": "Seleccionar idioma"
  },
  "favorites": {
    "title": "Canciones Favoritas",
    "empty": "Aún no hay canciones favoritas",
    "empty.description": "Haz clic en el icono de corazón en cualquier canción para añadirla a tus favoritos",
    "addToFavorites": "Añadir a favoritos",
    "removeFromFavorites": "Eliminar de favoritos"
  }
} 